//  Helper.swift
//  WFamily
//  Created by appentus technologies pvt. ltd. on 9/24/19.
//  Copyright © 2019 Bullet Apps. All rights reserved.



import Foundation
import UIKit

import MBProgressHUD
import CDAlertView
import SVProgressHUD


extension UIViewController {
    func func_ShowHud() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.view.isUserInteractionEnabled = false
    }
    
    func func_HideHud() {
        MBProgressHUD.hide(for: self.view, animated:true)
        self.view.isUserInteractionEnabled = true
    }
    
    func func_ShowHud_SV() {
        SVProgressHUD.show()
        self.view.isUserInteractionEnabled = false
    }
    
    func func_HideHud_SV() {
        SVProgressHUD.dismiss()
        self.view.isUserInteractionEnabled = true
    }
    
    func func_show_success(_ message:String) {
        let alert = CDAlertView(title:message, message:"", type:.success)
        alert.autoHideTime = 2
        alert.show()
    }
    
    func func_show_error(_ message:String) {
        let alert = CDAlertView(title:message, message:"", type:.error)
        alert.autoHideTime = 2
        alert.show()
    }
    
    
}



extension UIViewController {
    func push_To_VC(_ identifier:String) {
        let vcToPush = self.storyboard?.instantiateViewController(withIdentifier:identifier)
        self.navigationController?.pushViewController(vcToPush!, animated: true)
    }
    
    func pop_To_VC() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_Back_Pop(_ sender:Any) {
        pop_To_VC()
    }

}


