//
//  File.swift
//  WFamily
//
//  Created by appentus technologies pvt. ltd. on 9/25/19.
//  Copyright © 2019 Bullet Apps. All rights reserved.
//

import Foundation


class Model_Add_Tracker {
    static let shared = Model_Add_Tracker()
    
    var mobile_number = ""
    var tracking_user_name = ""
    
    var online_created = ""
    var offline_last_seen = ""
    var offline_created = ""
    
    var new_status = ""
    var arr_result = [Model_Add_Tracker]()
    var arr_weekly_day_reports = [Model_Add_Tracker]()
    
    func func_getReports(completionHandler:@escaping (String,String)->()) {
        let param = ["mobile_number":mobile_number]
        print(param)
        
        APIFunc.postAPI(url:kGetReports, parameters:param) {
            (dict_JSON) in
            print(dict_JSON)
            
            self.arr_result.removeAll()
            self.arr_weekly_day_reports.removeAll()
            
            if "\(dict_JSON["status"] ?? "")" == "1" {
                if let result =  dict_JSON["result"] as? [[String:Any]] {
                    for dict_result in result {
                        self.arr_result.append(self.func_set_data(dict_result))
                    }
                }
                
                if let weekly_day_reports =  dict_JSON["weekly_day_reports"] as? [[String:Any]] {
                    for dict_result in weekly_day_reports {
                        self.arr_weekly_day_reports.append(self.func_set_data(dict_result))
                    }
                }
                
                if let new_status =  dict_JSON["new_status"] as? String {
                    self.new_status = new_status
                }
                
                completionHandler("\(dict_JSON["status"] ?? "")","\(dict_JSON["message"] ?? "")")
            } else {
                completionHandler("\(dict_JSON["status"] ?? "")","\(dict_JSON["message"] ?? "")")
            }
        }
    }

    
    private func func_set_data(_ dict:[String:Any]) -> Model_Add_Tracker {
        let model = Model_Add_Tracker()
        
        model.online_created = "\(dict["online_created"] ?? "")"
        model.offline_last_seen = "\(dict["offline_last_seen"] ?? "")"
        model.offline_created = "\(dict["offline_created"] ?? "")"
        
        return model
    }
    
    
}



