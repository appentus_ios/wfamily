//  AddTrackerVC.swift
//  WFamily
//  Created by Bullet Apps on 17/06/19.
//  Copyright © 2019 Bullet Apps. All rights reserved.



import UIKit

var noOfItems : Int?
var monthName : String?
var TrackData : [Double]?



class AddTrackerVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var addNumberFirstBtn: UIButton?
    
    @IBOutlet weak var noNumberTrackView: UIView?
    @IBOutlet weak var numbersTrackView: UIView?
    @IBOutlet weak var deleteHistoryBtn: UIButton?
    @IBOutlet weak var nowBtn: UIButton?
    @IBOutlet weak var dailyBtn: UIButton?
    @IBOutlet weak var weeklyBtn: UIButton?
    
    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var btnVWHeightConstraint: NSLayoutConstraint?
    
    let arrOnlineTime = ["17:51:43","17:44:20","17:41:02","17:38:50","17:51:43","17:44:20","17:41:02","17:38:50"]
    let arrOfflineTime = ["17:52:46","17:45:05","17:43:14","17:40:51","17:52:46","17:45:05","17:43:14","17:40:51"]
    
    @IBOutlet weak var graphView: UIView?
    var examples: Examples!
    var chartGraphView: ScrollableGraphView!
    var currentGraphType = GraphType.multiOne
    
    @IBOutlet weak var view_Status:UIView!
    @IBOutlet weak var color_Status:UIView!
    @IBOutlet weak var lbl_Status:UILabel!
    
    var arr_reload_Reports = [Model_Add_Tracker]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        TrackData = [5.0,25.0,40.0,15.0,30.0,50.0,35.0,20.0,45.0,10.0]
        noOfItems = TrackData?.count
        
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLLL"
        let nameOfMonth = dateFormatter.string(from: now)
        print(nameOfMonth)
        monthName = nameOfMonth
        
        examples = Examples()
        chartGraphView = examples.createMultiPlotGraphTwo(CGRect.init(x: (self.graphView?.frame.origin.x)!, y: (self.graphView?.frame.origin.y)!, width: (self.graphView?.frame.size.width)! - 15, height: 200))
        
//        print(chartGraphView.frame.size.height)
        graphView?.insertSubview(chartGraphView, at: 0)
        
//        func_reload_Graph([0,0,0,0,0,0,0,0])
        
        color_Status.layer.cornerRadius = color_Status.bounds.height/2
        color_Status.clipsToBounds = true
        
        self.setUpUIElements()
        
        NotificationCenter.default.addObserver(self, selector: #selector(func_didBecomeActive), name: NSNotification.Name (rawValue: "didBecomeActive"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(func_Track_Status(not:)), name: NSNotification.Name (rawValue: "track_status"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(func_Track_reload), name: NSNotification.Name (rawValue: "track_Number"), object: nil)
    }
        
    @objc func func_didBecomeActive() {
        if is_Subscribing {
            if UserDefaults.standard.bool(forKey: kNumberTrack) {
                btnVWHeightConstraint?.constant = 60
                noNumberTrackView?.isHidden = true
                numbersTrackView?.isHidden = false
                addNumberFirstBtn?.setTitle(Model_Add_Tracker.shared.tracking_user_name, for: .normal)
                
                func_getReports()
            }
        }
                
    }
    
    func func_reload_Graph(_ trackData:[Double]) {
        TrackData = trackData
        noOfItems = TrackData?.count
        
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLLL"
        let nameOfMonth = dateFormatter.string(from: now)
        print(nameOfMonth)
        monthName = nameOfMonth
        
        examples = Examples()
        
        if chartGraphView != nil {
            chartGraphView.removeFromSuperview()
        }
            
        chartGraphView = examples.createMultiPlotGraphTwo(CGRect.init(x: (self.graphView?.frame.origin.x)!, y: (self.graphView?.frame.origin.y)!, width: (self.graphView?.frame.size.width)! - 15, height: 200))
//        print(chartGraphView.frame.size.height)
        
        graphView?.insertSubview(chartGraphView, at: 0)
        
//        chartGraphView.reload()
//        graphView?.reloadInputViews()
    }
    
    @objc func func_Track_reload() {
        let size = Utility.getIphoneDevice(vc: self)
        
        if size == iPhoneDeviceEnum.iPhone_5_5s_5c_SE.rawValue {
            addNumberFirstBtn?.titleLabel?.font = UIFont.init(name: usedFontName, size: 17.0)
            nowBtn?.titleLabel?.font = UIFont.init(name: usedFontName, size: 15.0)
            dailyBtn?.titleLabel?.font = UIFont.init(name: usedFontName, size: 15.0)
            weeklyBtn?.titleLabel?.font = UIFont.init(name: usedFontName, size: 15.0)
        }
        
        if is_Subscribing {
            if UserDefaults.standard.bool(forKey: kNumberTrack) {
                btnVWHeightConstraint?.constant = 60
                noNumberTrackView?.isHidden = true
                numbersTrackView?.isHidden = false
                addNumberFirstBtn?.setTitle(Model_Add_Tracker.shared.tracking_user_name, for: .normal)
                
                func_getReports()
            } else {
                btnVWHeightConstraint?.constant = 0
                noNumberTrackView?.isHidden = false
                numbersTrackView?.isHidden = true
                addNumberFirstBtn?.setTitle("", for: .normal)
                
                nowBtn?.isUserInteractionEnabled = false
                dailyBtn?.isUserInteractionEnabled = false
                weeklyBtn?.isUserInteractionEnabled = false
            }
        } else {
            btnVWHeightConstraint?.constant = 0
            noNumberTrackView?.isHidden = false
            numbersTrackView?.isHidden = true
            addNumberFirstBtn?.setTitle("", for: .normal)
            
            nowBtn?.isUserInteractionEnabled = false
            dailyBtn?.isUserInteractionEnabled = false
            weeklyBtn?.isUserInteractionEnabled = false
        }
    }
    
    @objc func func_Track_Status(not:Notification) {
        Model_Add_Tracker.shared.new_status = "\(not.object!)"
        self.lbl_Status.text = Model_Add_Tracker.shared.new_status
        if Model_Add_Tracker.shared.new_status.contains("On") {
            self.color_Status.backgroundColor = hexStringToUIColor(hex: "00F900")
        } else {
            self.color_Status.backgroundColor = hexStringToUIColor(hex: "F23000")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //MARK: UITableView DataSource & Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return arrOnlineTime.count
        return arr_reload_Reports.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let historyCell = tableView.dequeueReusableCell(withIdentifier: "historyCell", for: indexPath)
        
        let onlineImgView = historyCell.viewWithTag(10) as! UIImageView
        let onlineTimeLbl = historyCell.viewWithTag(20) as! UILabel
        let onlineDateLbl = historyCell.viewWithTag(30) as! UILabel
        let middleLbl = historyCell.viewWithTag(40) as! UILabel
        let offlineImgView = historyCell.viewWithTag(50) as! UIImageView
        let offlineTimeLbl = historyCell.viewWithTag(60) as! UILabel
        let offlineDateLbl = historyCell.viewWithTag(70) as! UILabel
        
        let size = Utility.getIphoneDevice(vc: self)
        if size == iPhoneDeviceEnum.iPhone_5_5s_5c_SE.rawValue {
            onlineTimeLbl.font = UIFont.init(name: usedFontName, size: 16.0)
            onlineDateLbl.font = UIFont.init(name: usedFontName, size: 9.0)
            middleLbl.font = UIFont.init(name: usedFontName, size: 14.0)
            offlineTimeLbl.font = UIFont.init(name: usedFontName, size: 16.0)
            offlineDateLbl.font = UIFont.init(name: usedFontName, size: 9.0)
        }
        
        onlineImgView.layer.cornerRadius = onlineImgView.frame.size.height/2
        offlineImgView.layer.cornerRadius = offlineImgView.frame.size.height/2
        
        let model = arr_reload_Reports[indexPath.row]
        let arr_Online_Created = model.online_created.components(separatedBy: " ")
        let arr_Offline_Created = model.offline_created.components(separatedBy: " ")
        
        onlineTimeLbl.text = arr_Online_Created[1]
        onlineDateLbl.text = arr_Online_Created[0]
        
        offlineTimeLbl.text = arr_Offline_Created[1]
        offlineDateLbl.text = arr_Offline_Created[0]
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        
        middleLbl.text = model.offline_created.offsetFrom(model.online_created)
        
        return historyCell
    }
    
    
    
    //MARK: Custom Methods
    func setUpUIElements() {
        addNumberFirstBtn?.layer.cornerRadius = (addNumberFirstBtn?.frame.size.height)!/2
        addNumberFirstBtn?.layer.borderColor = #colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1)
        addNumberFirstBtn?.layer.borderWidth = 1.0
        
        deleteHistoryBtn?.layer.cornerRadius = (deleteHistoryBtn?.frame.size.height)!/2
        
        nowBtn?.layer.cornerRadius = (nowBtn?.frame.size.height)!/2
        nowBtn?.layer.borderColor = #colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1)
        nowBtn?.layer.borderWidth = 1.0
        
        dailyBtn?.layer.cornerRadius = (dailyBtn?.frame.size.height)!/2
        dailyBtn?.layer.borderColor = #colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1)
        dailyBtn?.layer.borderWidth = 1.0
        
        weeklyBtn?.layer.cornerRadius = (weeklyBtn?.frame.size.height)!/2
        weeklyBtn?.layer.borderColor = #colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1)
        weeklyBtn?.layer.borderWidth = 1.0
    }
    
    //MARK: IBActions
    @IBAction func addNumberFirstButtonClicked(_ sender: UIButton) {
        
        /*
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            sender.backgroundColor = #colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1)
            sender.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            
            if let scroll = scrollPage {
                scroll(0)
            }
        }else {
            sender.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            sender.setTitleColor(#colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1), for: .normal)
        }
        */
    }
    
    @IBAction func deleteButtonClicked(_ sender: UIButton) {
       
    }
    
    @IBAction func nowButtonClicked(_ sender: UIButton) {
        
        //sender.isSelected = !sender.isSelected
        
        sender.backgroundColor = #colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1)
        sender.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        
        dailyBtn?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        dailyBtn?.setTitleColor(#colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1), for: .normal)
        
        weeklyBtn?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        weeklyBtn?.setTitleColor(#colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1), for: .normal)
        
        view_Status.isHidden = false
        tableView!.isHidden = true
    }
    
    @IBAction func dailyButtonClicked(_ sender: UIButton) {
        
        //sender.isSelected = !sender.isSelected
        
        sender.backgroundColor = #colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1)
        sender.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        
        nowBtn?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        nowBtn?.setTitleColor(#colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1), for: .normal)
        
        weeklyBtn?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        weeklyBtn?.setTitleColor(#colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1), for: .normal)
        
        arr_reload_Reports = Model_Add_Tracker.shared.arr_result
        tableView?.reloadData()
        tableView!.isHidden = false
        view_Status.isHidden = true
    }
    
    @IBAction func weeklyButtonClicked(_ sender: UIButton) {
        sender.backgroundColor = #colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1)
        sender.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        
        nowBtn?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        nowBtn?.setTitleColor(#colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1), for: .normal)
        
        dailyBtn?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        dailyBtn?.setTitleColor(#colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1), for: .normal)
        
        arr_reload_Reports = Model_Add_Tracker.shared.arr_weekly_day_reports
        tableView?.reloadData()
        tableView!.isHidden = false
        view_Status.isHidden = true
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    
    func func_getReports() {
        func_ShowHud()
        Model_Add_Tracker.shared.func_getReports { (status, message) in
            DispatchQueue.main.async {
                self.func_HideHud()
                if status == "1" {
                    self.nowBtn?.isUserInteractionEnabled = true
                    self.dailyBtn?.isUserInteractionEnabled = true
                    self.weeklyBtn?.isUserInteractionEnabled = true
                    
                    self.lbl_Status.text = Model_Add_Tracker.shared.new_status
                    if Model_Add_Tracker.shared.new_status.contains("On") {
                        self.color_Status.backgroundColor = hexStringToUIColor(hex: "00F900")
                    } else {
                        self.color_Status.backgroundColor = hexStringToUIColor(hex: "F23000")
                    }
                    
                    let arr_Full_reports = Model_Add_Tracker.shared.arr_weekly_day_reports+Model_Add_Tracker.shared.arr_result
                    var diff = [Double]()
                    for model in arr_Full_reports {
                        let str_Diff = model.offline_created.offsetFrom_report(model.online_created)
                        diff.append(Double(str_Diff)!)
                    }
                    
//                    self.func_reload_Graph(diff)
                    
                    let btn = UIButton()
                    self.nowButtonClicked(btn)
                }
            }
        }
    }
}



extension String {
    func offsetFrom(_ str_date : String) -> String {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let date = dateFormat.date(from:self)
        let offline_Date = dateFormat.date(from:str_date)
        
        let difference = NSCalendar.current.dateComponents([.day, .hour, .minute, .second], from: date!, to:offline_Date!)
        
        var seconds = "\(difference.second ?? 0)".replacingOccurrences(of: "-", with: "")
        if seconds.count == 1 {
            seconds = "0\(seconds)"
        }
        var minutes = "\(difference.minute ?? 0)".replacingOccurrences(of: "-", with: "")
        if minutes.count == 1 {
            minutes = "0\(minutes)"
        }
        
        return "\(minutes):\(seconds)"
        
        let hours = "\(difference.hour ?? 0)h" + " " + minutes
        let days = "\(difference.day ?? 0)d" + " " + hours
        
        if let day = difference.day, day          > 0 { return days }
        if let hour = difference.hour, hour       > 0 { return hours }
        if let minute = difference.minute, minute > 0 { return minutes }
        if let second = difference.second, second > 0 { return seconds }
        
        return ""
    }
    
    func offsetFrom_report(_ str_date : String) -> String {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let date = dateFormat.date(from:self)
        let offline_Date = dateFormat.date(from:str_date)
        
        let difference = NSCalendar.current.dateComponents([.day, .hour, .minute, .second], from: date!, to:offline_Date!)
        
        var seconds = "\(difference.second ?? 0)".replacingOccurrences(of: "-", with: "")
        let minutes = "\(difference.minute ?? 0)".replacingOccurrences(of: "-", with: "")
        
        return "\(minutes).\(seconds)"
    }
    
}
