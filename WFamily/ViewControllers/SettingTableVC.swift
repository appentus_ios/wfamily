//  SettingTableVC.swift
//  WFamily
//  Created by Bullet Apps on 18/06/19.
//  Copyright © 2019 Bullet Apps. All rights reserved.



import UIKit
import MessageUI
import UserNotifications


class SettingTableVC: UITableViewController,MFMailComposeViewControllerDelegate {
    @IBOutlet weak var uniqueIdLbl: UILabel?
    @IBOutlet weak var premiumView: UIView?
    @IBOutlet weak var getpremiumBtn: UIButton?
    
    @IBOutlet weak var settingView: UIView?
    @IBOutlet weak var contactUsBtn: UIButton?
    @IBOutlet weak var thanksUsBtn: UIButton?
    
    @IBOutlet weak var expireView: UIView?
    @IBOutlet weak var durationLbl: UILabel?
    @IBOutlet weak var expireLbl: UILabel?
    
    @IBOutlet weak var switch_notification: UISwitch?
    @IBOutlet weak var switch_notification_sound: UISwitch?
    @IBOutlet weak var switch_notification_offline: UISwitch?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUiElements()
        
        uniqueIdLbl?.text = Model_Splash.shared.id
        
        NotificationCenter.default.addObserver(self, selector: #selector(func_set_notification), name: NSNotification.Name (rawValue: "didBecomeActive"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        func_set_notification()
        
        durationLbl?.text = "\(duration_time) Premium Plus Member"
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        if expriryDate != nil {
            expireLbl?.text = "Expires on \(formatter.string(from:expriryDate))"
        }
        
        let size = Utility.getIphoneDevice(vc: self)
        if size == iPhoneDeviceEnum.iPhone_5_5s_5c_SE.rawValue {
            getpremiumBtn?.titleLabel?.font = UIFont.init(name: usedFontName, size: 18.0)
            contactUsBtn?.titleLabel?.font = UIFont.init(name: usedFontName, size: 18.0)
            thanksUsBtn?.titleLabel?.font = UIFont.init(name: usedFontName, size: 18.0)
            
            durationLbl?.font = UIFont.init(name: usedFontName, size: 12.0)
            expireLbl?.font = UIFont.init(name: usedFontName, size: 9.0)
            
            getpremiumBtn?.imageEdgeInsets = UIEdgeInsets.init(top: 0, left: -100, bottom: 0, right: 0)
        }
        
        if size == iPhoneDeviceEnum.iPhone_6_6s_7_8.rawValue {
            getpremiumBtn?.imageEdgeInsets = UIEdgeInsets.init(top: 0, left: -120, bottom: 0, right: 0)
        }
        
        if UserDefaults.standard.bool(forKey: kPremiumTrack) {
             expireView?.isHidden = false
             getpremiumBtn?.backgroundColor = .clear
             self.getpremiumBtn?.setTitle("", for: .normal)
             self.getpremiumBtn?.setImage(UIImage(), for: .normal)
        } else {
            expireView?.isHidden = true
            getpremiumBtn?.backgroundColor = #colorLiteral(red: 0.8386928439, green: 0.1190194711, blue: 0.4354643822, alpha: 1)
            self.getpremiumBtn?.setTitle("Get Premium", for: .normal)
            self.getpremiumBtn?.setImage(UIImage.init(named: "setting_getpremium_icon"), for: .normal)
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
        if tableView.contentSize.height > self.view.frame.size.height {
            tableView.isScrollEnabled = true
        }else {
            tableView.isScrollEnabled = false
        }
    }
    
    //MARK: Custom Methods
    func setUiElements() {
        expireView?.isHidden = true
        expireLbl?.layer.cornerRadius = (expireLbl?.frame.size.height)!/2
        
        premiumView?.layer.cornerRadius = cornerRadius
        settingView?.layer.cornerRadius = cornerRadius
        
        // Shadow and Radius for Button
        contactUsBtn?.layer.shadowColor = UIColor.black.cgColor
        contactUsBtn?.layer.shadowOpacity = shadowOpacity
        contactUsBtn?.layer.shadowRadius = shadowRadius
        contactUsBtn?.layer.shadowOffset = CGSize.zero
        contactUsBtn?.layer.masksToBounds = false
        
        // Shadow and Radius for Button
        thanksUsBtn?.layer.shadowColor = UIColor.black.cgColor
        thanksUsBtn?.layer.shadowOpacity = shadowOpacity
        thanksUsBtn?.layer.shadowRadius = shadowRadius
        thanksUsBtn?.layer.shadowOffset = CGSize.zero
        thanksUsBtn?.layer.masksToBounds = false
        
        getpremiumBtn?.layer.cornerRadius = cornerRadius
        contactUsBtn?.layer.cornerRadius = cornerRadius
        thanksUsBtn?.layer.cornerRadius = cornerRadius
    }
    
    //MARK: IBActions
    @IBAction func copyClipboardButtonClicked(_ sender: UIButton) {
        UIPasteboard.general.string = uniqueIdLbl?.text
        self.showToast(message: "Your ID copied to Clipboard!", seconds: 1.0)
    }
    
    @IBAction func getPremiumButtonClicked(_ sender: UIButton) {
        if getpremiumBtn?.titleLabel?.text == "Get Premium" {
            let vc = Utility.loadViewControllerFromSettingStoryBoard(identifier: "SingleNumberVC") as! SingleNumberVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func contactUsButtonClicked(_ sender: UIButton) {
        
        if MFMailComposeViewController.canSendMail() {
            let mailVC = MFMailComposeViewController()
            mailVC.mailComposeDelegate = self
            mailVC.setToRecipients(["vivekwarde@icloud.com"])
            mailVC.setSubject("WFamily App Problem!")
            mailVC.setMessageBody("Type here...", isHTML: false)
            present(mailVC, animated: true, completion: nil)
        } else {
            self.showaAlert(message: "Mail Service are not available!")
        }
        
    }
    
    @IBAction func thanksUsButtonClicked(_ sender: UIButton) {
        //"https://itunes.apple.com/us/app/whats-tracker/id1410790509?ls=1&mt=8"
        
        if let appStoreURL = URL.init(string: "https://itunes.apple.com/us/app/WFamily/id\(KAppStoreID)?ls=1&mt=8") {
            if UIApplication.shared.canOpenURL(appStoreURL) {
                UIApplication.shared.open(appStoreURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    @IBAction func notificationSwitchToggled(_ sender: UISwitch) {
        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
    }
    
    @IBAction func notificationSoundSwitchToggled(_ sender: UISwitch) {
        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
    }
    
    @IBAction func offlineNotificationSwitchToggled(_ sender: UISwitch) {
        UserDefaults.standard.set(sender.isOn, forKey: "notification_offline")
    }
    

    //MARK: MFMailComposeViewController Delegates
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    
}



extension SettingTableVC {
    @objc func func_set_notification() {
        let center = UNUserNotificationCenter.current()
        center.getNotificationSettings { (settings) in
//            print(settings)
            if(settings.authorizationStatus == .authorized) {
                print("Push authorized")
                DispatchQueue.main.async {
                    self.switch_notification!.isOn = true
                }
                if(settings.soundSetting == .enabled) {
                    print("sound enabled")
                    DispatchQueue.main.async {
                        self.switch_notification_sound!.isOn = true
                    }
                } else {
                    print("sound disabled")
                    DispatchQueue.main.async {
                        self.switch_notification_sound!.isOn = false
                    }
                }
                
                if let notification_offline = UserDefaults.standard.object(forKey:"notification_offline") as? Bool {
                    DispatchQueue.main.async {
                        self.switch_notification_offline!.isOn = notification_offline
                    }
                } else {
                    DispatchQueue.main.async {
                        self.switch_notification_offline!.isOn = false
                    }
                }
            } else {
                print("Push not authorized")
                DispatchQueue.main.async {
                    self.switch_notification!.isOn = false
                    self.switch_notification_sound!.isOn = false
                    self.switch_notification_offline!.isOn = false
                    
                    self.switch_notification_sound!.isUserInteractionEnabled = false
                    self.switch_notification_offline!.isUserInteractionEnabled = false
                }
            }
        }
    }
}

