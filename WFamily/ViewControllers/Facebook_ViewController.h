////
////  Facebook_ViewController.h
////  WFamily
////
////  Created by appentus technologies pvt. ltd. on 10/17/19.
////  Copyright © 2019 Bullet Apps. All rights reserved.
////
//
//#import <UIKit/UIKit.h>
//#import <FBAudienceNetwork/FBAudienceNetwork.h>
////#import <FBSDKCoreKit/FBSDKCoreKit.h>
//
//@import FBAudienceNetwork;
//
//
//NS_ASSUME_NONNULL_BEGIN
//
//@interface Facebook_ViewController : UIViewController <FBAdViewDelegate>
//
//@property (weak, nonatomic) IBOutlet UIView *adContainer;
//@property (nonatomic, strong) FBAdView *adView;
//
//@end
//
//NS_ASSUME_NONNULL_END
