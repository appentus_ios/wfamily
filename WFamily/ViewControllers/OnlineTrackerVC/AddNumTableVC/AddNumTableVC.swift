//
//  AddNumTableVC.swift
//  WFamily
//
//  Created by Bullet Apps on 18/06/19.
//  Copyright © 2019 Bullet Apps. All rights reserved.
//

import UIKit
import Alamofire
import ContactsUI

let apdel = UIApplication.shared.delegate as! AppDelegate

var disableTimerWithTrackBtn : (() -> Void)?
var resumeTimerWithLastTrackTime : ((Int) -> Void)?


var is_Subscribing = false


var product_id_Weekly = "weekly_111"
var product_id_Monthly = "monthly_222"
var product_id_3_Monthly = "3month_333"
var product_id_purchased = ""


class AddNumTableVC: UITableViewController,MICountryPickerDelegate,CNContactPickerDelegate {
    @IBOutlet weak var trackView1: UIView?
    @IBOutlet weak var countryImgView1: UIImageView?
    @IBOutlet weak var countryCodeBtn1: UIButton?
    @IBOutlet weak var countryCodeLbl1: UILabel?
    @IBOutlet weak var txtFieldName1: UITextField!
    @IBOutlet weak var txtFieldNumber1: UITextField?
    @IBOutlet weak var GetPremiumBtn1: UIButton?
    @IBOutlet weak var GetPremiumView1: UIView?
    @IBOutlet weak var tryFreeLbl1: UILabel?
    @IBOutlet weak var trackLbl1: UILabel?
    @IBOutlet weak var contactListBtn: UIButton?
    @IBOutlet weak var dialCodeBtn: UIButton?
    @IBOutlet weak var timerImgView: UIImageView?
    @IBOutlet weak var trackView3: UIView?
    @IBOutlet weak var GetPremiumBtn3: UIButton?
    
    @IBOutlet weak var unlimitedTrackingBtn: UIButton?
    @IBOutlet weak var detailedStatisticsBtn: UIButton?
    @IBOutlet weak var fasterNotificationBtn: UIButton?
    @IBOutlet weak var supportBtn: UIButton?
    
    @IBOutlet weak var view_purchased_plan: UIView?
    
    @IBOutlet weak var planDurationLbl:UILabel!
    @IBOutlet weak var planDetailLbl:UILabel!
    
    
    let bundle = "assets.bundle/"
    var dialCode1,dialCode2 : String?
    var countryCode1,countryCode2 : String?
    var pickerTag: Int?
    
    var planDuration : String?
    var planDetail : String?
    
    var product_id = ""
    
    var pop_Confirm_VC:POPUP_Confirm_ViewController!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(receiptValidation), name: NSNotification.Name (rawValue: "FCMToken"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(func_hide_confirm), name: NSNotification.Name (rawValue: "hide_confirm"), object: nil)
        self.setUiElements()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        func_set_data()
        
        view_purchased_plan?.layer.cornerRadius = cornerRadius
        view_purchased_plan?.clipsToBounds = true
        
        view_purchased_plan?.isHidden = true
        
        let size = Utility.getIphoneDevice(vc: self)
        
        if size == iPhoneDeviceEnum.iPhone_5_5s_5c_SE.rawValue {
            GetPremiumBtn1?.titleLabel?.font = UIFont.init(name: usedFontName, size: 20.0)
            unlimitedTrackingBtn?.titleLabel?.font = UIFont.init(name: usedFontName, size: 10.0)
            detailedStatisticsBtn?.titleLabel?.font = UIFont.init(name: usedFontName, size: 10.0)
            fasterNotificationBtn?.titleLabel?.font = UIFont.init(name: usedFontName, size: 10.0)
            supportBtn?.titleLabel?.font = UIFont.init(name: usedFontName, size: 10.0)
        }
        
        if pickerTag == 1 {
            countryCodeLbl1?.text = countryCode1 ?? "IN"
            countryCodeBtn1?.setTitle(dialCode1 ?? "+91", for: .normal)
            countryImgView1?.image = UIImage(named: bundle + (countryCode1?.lowercased() ?? "in") + ".png", in: Bundle(for: MICountryPicker.self), compatibleWith: nil)
        }
        
        self.hideKeyboardWhenTappedAround()
    }
    
    func func_set_data() {
        if UserDefaults.standard.bool(forKey: kNumberTrack) {
            if UserDefaults.standard.bool(forKey: kTrialExpired) {
                    DispatchQueue.main.async {
                        self.GetPremiumBtn1?.backgroundColor = .gray
                        self.GetPremiumBtn1?.setTitle("Trial Expired", for: .normal)
                    }
                
                    self.tryFreeLbl1?.isHidden = true
                    self.trackLbl1?.isHidden = true
                    self.timerImgView?.isHidden = true
                
                    self.txtFieldName1.isUserInteractionEnabled = false
                    self.txtFieldNumber1?.isUserInteractionEnabled = false
                    self.contactListBtn?.isUserInteractionEnabled = false
                    self.dialCodeBtn?.isUserInteractionEnabled = false
                } else {
                DispatchQueue.main.async {
                    self.GetPremiumBtn1?.backgroundColor = #colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1)
                }
                
                self.tryFreeLbl1?.text = "Stop Tracking"
                is_Subscribing = true
                
                self.trackLbl1?.text = "00:00:00"
                
                self.txtFieldName1.isUserInteractionEnabled = false
                self.txtFieldNumber1?.isUserInteractionEnabled = false
                self.contactListBtn?.isUserInteractionEnabled = false
                self.dialCodeBtn?.isUserInteractionEnabled = false
                
                if let trackData = UserDefaults.standard.dictionary(forKey: kTrackDict) as? [String:String] {
                    txtFieldName1.text = trackData["trackName"]
                    txtFieldNumber1?.text = trackData["trackNumber"]
                    countryCodeBtn1?.setTitle(trackData["trackCountryCode"], for: .normal)
                    countryCodeLbl1?.text = trackData["trackCountry"]
                }
            }
        } else {
            DispatchQueue.main.async {
                self.GetPremiumBtn1?.backgroundColor = #colorLiteral(red: 0.8386928439, green: 0.1190194711, blue: 0.4354643822, alpha: 1)
            }
            
            is_Subscribing = false
            
            self.tryFreeLbl1?.text = "Start Tracking"
            self.trackLbl1?.text = "Track Any Number For 8 Hours"
            
            self.txtFieldName1.isUserInteractionEnabled = true
            self.txtFieldNumber1?.isUserInteractionEnabled = true
            self.contactListBtn?.isUserInteractionEnabled = true
            self.dialCodeBtn?.isUserInteractionEnabled = true
        }
        
        if UserDefaults.standard.bool(forKey: kPremiumTrack) {
            view_purchased_plan?.isHidden = false
            
            switch product_id {
            case product_id_Weekly:
                planDuration = "FOR 1 WEEK"
                planDetail = "Track Single Number For Full Week"
            case product_id_Monthly:
                planDuration = "FOR 1 MONTH"
                planDetail = "Track Single Number For Full Month"
            default:
                planDuration = "FOR 3 MONTHS"
                planDetail = "Track Single Number For Three Months"
            }
            
            planDurationLbl.text = planDuration ?? ""
            planDetailLbl.text = planDetail ?? ""
        }
        
        tableView.reloadData()
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        
        disableTimerWithTrackBtn = {
            
            DispatchQueue.main.async {
                self.GetPremiumBtn1?.backgroundColor = .gray
//                self.GetPremiumBtn1?.setTitle("Trial Expired", for: .normal)
            }
            
//            self.tryFreeLbl1?.isHidden = true
//            self.trackLbl1?.isHidden = true
//            self.timerImgView?.isHidden = true
//
//            self.txtFieldName1.isUserInteractionEnabled = true
//            self.txtFieldNumber1?.isUserInteractionEnabled = true
//            self.contactListBtn?.isUserInteractionEnabled = true
//            self.dialCodeBtn?.isUserInteractionEnabled = true
        }
        
        resumeTimerWithLastTrackTime = { (seconds) in
            apdel.sec = seconds
            apdel.timer?.invalidate()
            apdel.timer = nil
            
            apdel.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.startTimer), userInfo: nil, repeats: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        let str_mobile_number = txtFieldNumber1!.text ?? ""
        var str_code = self.countryCodeBtn1?.titleLabel?.text ?? ""
        str_code = str_code.replacingOccurrences(of: "+", with: "")
        
        Model_Add_Tracker.shared.mobile_number = str_code+str_mobile_number.trimmingCharacters(in:.whitespaces)
        Model_Add_Tracker.shared.tracking_user_name = txtFieldName1.text!
        
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "track_Number"), object: nil)
    }
    
    //MARK: Custom Methods
    func setUiElements() {
        // Shadow and Radius for Button
        GetPremiumView1?.layer.shadowColor = UIColor.black.cgColor
        GetPremiumView1?.layer.shadowOpacity = shadowOpacity
        GetPremiumView1?.layer.shadowRadius = shadowRadius
        GetPremiumView1?.layer.shadowOffset = CGSize.zero
        GetPremiumView1?.layer.masksToBounds = false
        
        DispatchQueue.main.async {
            self.trackView1?.layer.cornerRadius = cornerRadius
            self.trackView3?.layer.cornerRadius = cornerRadius
            
            self.GetPremiumView1?.layer.cornerRadius = cornerRadius
            self.GetPremiumBtn1?.layer.cornerRadius = cornerRadius
            self.GetPremiumBtn3?.layer.cornerRadius = cornerRadius
        }
        
        countryCodeLbl1?.text = countryCode1 ?? "IN"
        countryCodeBtn1?.setTitle(dialCode1 ?? "+91", for: .normal)
        countryImgView1?.image = UIImage(named: bundle + (countryCode1?.lowercased() ?? "in") + ".png", in: Bundle(for: MICountryPicker.self), compatibleWith: nil)
    }
    
    @objc func startTimer() {
        apdel.sec += 1
//        print("apdel.sec \(apdel.sec)")
        
        let (h,m,s) = timeCalculation(time: apdel.sec)
        
        let minut = apdel.sec / 60
//        print(minut)
        
        trackLbl1?.text = "\(h):\(m):\(s)"
        
        guard minut >= 480 else {
            return
        }
        
        if let disable = disableTimerWithTrackBtn {
            UserDefaults.standard.set(true, forKey: kTrialExpired)
            
            apdel.sec = 0
            apdel.totalSeconds = 0
            apdel.timer?.invalidate()
            
            disable()
        }
        
    }
    
    func timeCalculation(time:Int) -> (String,String,String) {
        let hours = Int(apdel.sec) / 3600
        let minutes = Int(apdel.sec) / 60 % 60
        let seconds = Int(apdel.sec) % 60
        
        apdel.totalSeconds = (Int(apdel.sec) % 60) + (minutes * 60)
//        print("apdel.totalSeconds \(apdel.totalSeconds)")
        
        return (String.init(format: "%02i",hours),String.init(format: "%02i",minutes),String.init(format: "%02i",seconds))
    }
    
    //MARK: IBActions
    @IBAction func btnCountryCodeClicked(_ sender: UIButton) {
        pickerTag = sender.tag
        
        let picker = MICountryPicker { (name, code) -> () in
            //print(code)
        }
        
        // Optional: To pick from custom countries list
        // picker.customCountriesCode = ["EG", "US", "AF", "AQ", "AX"]
        
        // delegate
        picker.delegate = self
        
        // Display calling codes
        // picker.showCallingCodes = true
        
        // or closure
        picker.didSelectCountryClosure = { name, code in
            // picker.navigationController?.popViewController(animated: true)
            picker.dismiss(animated: true, completion: nil)
        }
        
        // navigationController?.pushViewController(picker, animated: true)
        self.present(picker, animated: true, completion: nil)
    }
    
    @IBAction func getPremiumButtonClicked(_ sender: UIButton) {
        if GetPremiumBtn1?.titleLabel?.text == "Trial Expired" {
            let vc = Utility.loadViewControllerFromSettingStoryBoard(identifier: "SingleNumberVC") as! SingleNumberVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            if !UserDefaults.standard.bool(forKey: kNumberTrack) {
                guard txtFieldName1?.text?.count ?? 0 > 0 else {
                    self.showaAlert(message: "Please Enter Name To Track!")
                    return
                }
                
                guard txtFieldNumber1?.text?.count ?? 0 > 0 else {
                    self.showaAlert(message: "Please Enter Number To Track!")
                    return
                }
                
                let storyboard = UIStoryboard (name: "Main", bundle: nil)
                pop_Confirm_VC = storyboard.instantiateViewController(withIdentifier: "POPUP_Confirm_ViewController") as? POPUP_Confirm_ViewController
                
                let str_mobile_number = txtFieldNumber1!.text ?? ""
                let str_code = self.countryCodeBtn1?.titleLabel?.text ?? ""
                
                let phone_NO = str_mobile_number.removeSpecialCharsFromString()//.trimmingCharacters(in:.whitespaces)
                let country_code = str_code
                
                pop_Confirm_VC.str_Mobile_Number = "\(country_code) \(phone_NO)"
                pop_Confirm_VC.delegate = self
                
                self.addChild(pop_Confirm_VC)
                self.view.addSubview(pop_Confirm_VC.view)
                
//                self.showAlert("Please Check & Confirm This Number.", message: "\(countryCodeBtn1?.titleLabel?.text ?? "") \(txtFieldNumber1?.text ?? "")", alertButtonTitles: ["Edit Number","Confirm"], alertButtonStyles: [.default,.default], vc: self) { (index) in
//
//                    if index == 0 {
//                        self.txtFieldNumber1?.becomeFirstResponder()
//                        self.dismiss(animated: true, completion: nil)
//                    } else {
//                        apdel.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.startTimer), userInfo: nil, repeats: true)
//
//                        self.GetPremiumBtn1?.backgroundColor = #colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1)
//                        self.tryFreeLbl1?.text = "Stop Tracking"
//                        self.trackLbl1?.text = "00:00:00"
//
//                        self.txtFieldName1.isUserInteractionEnabled = false
//                        self.txtFieldNumber1?.isUserInteractionEnabled = false
//                        self.contactListBtn?.isUserInteractionEnabled = false
//                        self.dialCodeBtn?.isUserInteractionEnabled = false
//
//                        let TrackDict = ["trackName"        : self.txtFieldName1.text,
//                                         "trackNumber"      : self.txtFieldNumber1?.text,
//                                         "trackCountryCode" : self.countryCodeBtn1?.titleLabel?.text,
//                                         "trackCountry"     : self.countryCodeLbl1?.text]
//
//                        UserDefaults.standard.set(TrackDict, forKey: kTrackDict)
//                        UserDefaults.standard.set(true, forKey: kNumberTrack)
//
//                        self.subscribeUser()
//                    }
//                }
            } else {
                apdel.timer?.invalidate()
                apdel.timer = nil
                
                self.GetPremiumBtn1?.backgroundColor = #colorLiteral(red: 0.8386928439, green: 0.1190194711, blue: 0.4354643822, alpha: 1)
                self.tryFreeLbl1?.text = "Start Tracking"
                is_Subscribing = false
                
                self.trackLbl1?.text = "Track Any Number For 8 Hours"
                
                self.txtFieldName1.isUserInteractionEnabled = true
                self.txtFieldNumber1?.isUserInteractionEnabled = true
                self.contactListBtn?.isUserInteractionEnabled = true
                self.dialCodeBtn?.isUserInteractionEnabled = true
                
                UserDefaults.standard.removeSuite(named: kTrackDict)
                UserDefaults.standard.removeObject(forKey: kNumberTrack)
                
                apdel.timer?.invalidate()
                apdel.timer = nil
                apdel.totalSeconds = 0
                apdel.sec = 0
                
                func_UnSubscribeUser()
            }
        }
    }
    
    
    
    @IBAction func getPremiumButton3Clicked(_ sender: UIButton) {
        let vc = Utility.loadViewControllerFromSettingStoryBoard(identifier: "SingleNumberVC") as! SingleNumberVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func contactListButtonClicked(_ sender: UIButton) {
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        contactPicker.displayedPropertyKeys =
            [CNContactGivenNameKey
                , CNContactPhoneNumbersKey]
        self.present(contactPicker, animated: true, completion: nil)
    }
    
    @IBAction func addMoreNumberButtonClicked(_ sender: UIButton) {
        
    }
    
    //MARK: ContactPicker Delegate
    func contactPicker(_ picker: CNContactPickerViewController,
                       didSelect contactProperty: CNContactProperty) {
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        // You can fetch selected name and number in the following way
        
        // user name
        let userName:String = contact.givenName
        print(userName)
        
        // user phone number
        let userPhoneNumbers:[CNLabeledValue<CNPhoneNumber>] = contact.phoneNumbers
        
        guard userPhoneNumbers.count > 0 else {
            return
        }
        
        let firstPhoneNumber:CNPhoneNumber = userPhoneNumbers[0].value
        
        // user phone number string
        var primaryPhoneNumberStr:String = firstPhoneNumber.stringValue
//        print(primaryPhoneNumberStr)
        
        let countryCode = firstPhoneNumber.value(forKey: "countryCode") as? String
//        print(countryCode)
        let getCountryDialCode = getCountryPhonceCode((countryCode?.uppercased())!)
//        print("countryCode : \(countryCode!), getCountryDialCode : \(getCountryDialCode)")
        
        countryCodeBtn1?.setTitle(dialCode1 ?? "+\(getCountryDialCode)", for: .normal)
        txtFieldName1.text = userName
        primaryPhoneNumberStr = primaryPhoneNumberStr.replacingOccurrences(of: "+\(getCountryDialCode)", with: "")
        txtFieldNumber1?.text = primaryPhoneNumberStr.replacingOccurrences(of: " ", with: "")
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        
    }
    
    //MARK: MICountryPicker Delegates
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String) {
        
    }
    
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        if pickerTag == 1 {
            self.dialCode1 = dialCode
            self.countryCode1 = code
        }else {
            self.dialCode2 = dialCode
            self.countryCode2 = code
        }
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return UserDefaults.standard.bool(forKey: kPremiumTrack) ? 1 : 2
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    
    func subscribeUser() {
        let str_mobile_number = txtFieldNumber1!.text ?? ""
        var str_code = self.countryCodeBtn1?.titleLabel?.text ?? ""
        str_code = str_code.replacingOccurrences(of: "+", with: "")
        
        let parameter = [
            "user_id":Model_Splash.shared.id,
            "mobile_number":str_mobile_number.removeSpecialCharsFromString(), //.trimmingCharacters(in:.whitespaces),
            "name":txtFieldName1!.text ?? "",
            "code":str_code] as [String : Any]
        print(parameter)
        
        let headers = ["password" : "123456"]
        
        func_ShowHud()
        Alamofire.request(kSubscribeUser, method: .post, parameters: parameter,headers: headers).responseJSON { (response) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                switch response.result {
                case .success:
                    let dict = response.result.value as! [String:Any]
                    print(dict)
                    
                    let arr_result = dict["result"] as! [[String:Any]]
                    
                    Model_OnlineTracker.shared.name =  "\(arr_result[0]["name"] ?? "")"
                    Model_OnlineTracker.shared.mobile_number = "\(arr_result[0]["mobile_number"] ?? "")"
                    Model_OnlineTracker.shared.id = "\(arr_result[0]["id"] ?? "")"
                    Model_OnlineTracker.shared.device_id = "\(arr_result[0]["device_id"] ?? "")"
                    Model_OnlineTracker.shared.token = "\(arr_result[0]["token"] ?? "")"
                    Model_OnlineTracker.shared.device_type = "\(arr_result[0]["device_type"] ?? "")"
                    Model_OnlineTracker.shared.plan_id = "\(arr_result[0]["plan_id"] ?? "")"
                    Model_OnlineTracker.shared.start = "\(arr_result[0]["start"] ?? "")"
                    Model_OnlineTracker.shared.end = "\(arr_result[0]["end"] ?? "")"
                    
                    break
                case .failure(let error):
                    self.func_show_error(error.localizedDescription)
                    print(error.localizedDescription)
                    break
                }
                
            }
        }
    }
    
    
    
    func func_UnSubscribeUser() {
        let str_mobile_number = txtFieldNumber1!.text ?? ""
        var str_code = self.countryCodeBtn1?.titleLabel?.text ?? ""
        str_code = str_code.replacingOccurrences(of: "+", with: "")
        
        let parameter = [
            "user_id":Model_Splash.shared.id,
            "mobile_number":str_code+str_mobile_number.removeSpecialCharsFromString()]  as [String : Any] //.trimmingCharacters(in:.whitespaces)] as [String : Any]
        print(parameter)
        
        let headers = ["password" : "123456"]
        
        func_ShowHud()
        Alamofire.request(kUnSubscribeUser, method: .post, parameters: parameter,headers: headers).responseJSON { (response) in
            DispatchQueue.main.async {
                self.func_HideHud()
                
                switch response.result {
                case .success:
                    let dict = response.result.value as! [String:Any]
                    print(dict)
                    
                    let status = "\(dict["status"] ?? "")"
                    let message = "\(dict["message"] ?? "")"
                    
                    break
                case .failure(let error):
                    print(error.localizedDescription)
                    break
                }
            }
        }
    }
    
    func func_GetSubscriber() {
        let parameter = ["user_id":Model_Splash.shared.id]
        print(parameter)
        
        let headers = ["password" : "123456"]
        
        Alamofire.request(kGetSubscriber, method: .post, parameters: parameter,headers: headers).responseJSON { (response) in
            switch response.result {
            case .success:
                let dict = response.result.value as! [String:Any]
                print(dict)
                
                if let arr_result = dict["result"] as? [[String:Any]] {
                    Model_OnlineTracker.shared.name =  "\(arr_result[0]["name"] ?? "")"
                    Model_OnlineTracker.shared.mobile_number = "\(arr_result[0]["mobile_number"] ?? "")"
                    Model_OnlineTracker.shared.id = "\(arr_result[0]["id"] ?? "")"
                    Model_OnlineTracker.shared.device_id = "\(arr_result[0]["device_id"] ?? "")"
                    Model_OnlineTracker.shared.token = "\(arr_result[0]["token"] ?? "")"
                    Model_OnlineTracker.shared.device_type = "\(arr_result[0]["device_type"] ?? "")"
                    Model_OnlineTracker.shared.plan_id = "\(arr_result[0]["plan_id"] ?? "")"
                    Model_OnlineTracker.shared.start = "\(arr_result[0]["start"] ?? "")"
                    Model_OnlineTracker.shared.end = "\(arr_result[0]["end"] ?? "")"
                }
                break
            case .failure(let error):
                print(error.localizedDescription)
                break
            }
        }
    }
    
    @objc func receiptValidation() {
        #if DEBUG
            let verifyReceiptURL = "https://sandbox.itunes.apple.com/verifyReceipt"
        #else
            let verifyReceiptURL = "https://buy.itunes.apple.com/verifyReceipt"
        #endif
        
        let receiptFileURL = Bundle.main.appStoreReceiptURL
        let receiptData = try? Data(contentsOf: receiptFileURL!)
        if receiptData == nil {
            return
        }
        
        let recieptString = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        let jsonDict: [String: AnyObject] = ["receipt-data" : recieptString! as AnyObject, "password" : "dab3f8e770384d99ae7dda0096529a30" as AnyObject]
        do {
            let requestData = try JSONSerialization.data(withJSONObject: jsonDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            guard let storeURL = URL(string: verifyReceiptURL) else { return }
            
            var storeRequest = URLRequest(url: storeURL)
            storeRequest.httpMethod = "POST"
            storeRequest.httpBody = requestData
            
            let session = URLSession(configuration: URLSessionConfiguration.default)
            let task = session.dataTask(with: storeRequest, completionHandler: { [weak self] (data, response, error) in
                if data == nil {
                    self!.func_show_error(error!.localizedDescription)
                    return
                }
                
                do {
                    guard let jsonResponse = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? Any else { return }
                    guard let dict_JSON = jsonResponse as? [String:Any] else { return }
                    guard let dict_receipt = dict_JSON["receipt"] as? [String:Any] else { return }
                    guard let arr_in_app = dict_receipt["in_app"] as? [[String:Any]]  else { return }
                    
                    if arr_in_app.count != 0 {
                        let dict_in_app = arr_in_app[0]
                        
                        self!.product_id = "\(dict_in_app["product_id"]!)"
                        let original_purchase_date = "\(dict_in_app["original_purchase_date"]!)"
                        
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
                        
                        if let original_purchase_dates = dict_in_app["original_purchase_date"] as? String {
                            let original_purchase_date = formatter.date(from: original_purchase_dates)
                            
                            if self!.product_id == product_id_Weekly {
                                duration_time = "7 days"
                                let cal = Calendar.current
                                expriryDate = cal.date(byAdding:.day, value:7, to:original_purchase_date!)!
                            } else if self!.product_id == product_id_Monthly {
                                duration_time = "1 month"
                                let cal = Calendar.current
                                expriryDate = cal.date(byAdding:.month, value:1, to:original_purchase_date!)!
                            } else if self!.product_id == product_id_3_Monthly {
                                duration_time = "3 months"
                                let cal = Calendar.current
                                expriryDate = cal.date(byAdding:.month, value:3, to:original_purchase_date!)!
                            }
                            
                            let exire_time =  expriryDate.offsetFrom(date: original_purchase_date!)
                            if exire_time == "0" {
                                UserDefaults.standard.set(true, forKey: kTrialExpired)
                                UserDefaults.standard.set(false, forKey: kPremiumTrack)
                            } else {
                                UserDefaults.standard.set(false, forKey: kTrialExpired)
                                UserDefaults.standard.set(true, forKey: kPremiumTrack)
                            }
                        }
                    }
                    
                    DispatchQueue.main.async {
                        self!.func_set_data()
                        self?.tableView.reloadData()
                    }
                } catch let parseError {
                    print(parseError)
                    DispatchQueue.main.async {
                        self!.func_set_data()
                    }
                }
            })
            task.resume()
        } catch let parseError {
            print(parseError)
            DispatchQueue.main.async {
                self.func_set_data()
            }
        }
    }
    
    
}



extension AddNumTableVC : POPUP_Delegate {
    @objc func func_hide_confirm() {
        if pop_Confirm_VC != nil {
            if pop_Confirm_VC.view != nil {
                pop_Confirm_VC.view.removeFromSuperview()
                pop_Confirm_VC.view = nil
            }
            
        }
        
    }
    
    func func_EditNumber() {
        self.txtFieldNumber1?.becomeFirstResponder()
        self.dismiss(animated: true, completion: nil)
    }
    
    func func_Confirm() {
        if Model_Splash.shared.id.isEmpty {
            funcAlertAPIIsNotWorkgin()
            return
        }
        
        apdel.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.startTimer), userInfo: nil, repeats: true)
        
        self.GetPremiumBtn1?.backgroundColor = #colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1)
        self.tryFreeLbl1?.text = "Stop Tracking"
        
        is_Subscribing = true
        
        self.trackLbl1?.text = "00:00:00"
        
        self.txtFieldName1.isUserInteractionEnabled = false
        self.txtFieldNumber1?.isUserInteractionEnabled = false
        self.contactListBtn?.isUserInteractionEnabled = false
        self.dialCodeBtn?.isUserInteractionEnabled = false
        
        let TrackDict = ["trackName"        : self.txtFieldName1.text,
                         "trackNumber"      : self.txtFieldNumber1?.text,
                         "trackCountryCode" : self.countryCodeBtn1?.titleLabel?.text,
                         "trackCountry"     : self.countryCodeLbl1?.text]
        
        UserDefaults.standard.set(TrackDict, forKey: kTrackDict)
        UserDefaults.standard.set(true, forKey: kNumberTrack)
        
        if !Model_Splash.shared.id.isEmpty {
            subscribeUser()
        }
    }
    
}



// MARK: - get Matched results using methods
func getCountryPhonceCode (_ country : String) -> String {
    var countryDictionary  = ["AF":"93",
                              "AL":"355",
                              "DZ":"213",
                              "AS":"1",
                              "AD":"376",
                              "AO":"244",
                              "AI":"1",
                              "AG":"1",
                              "AR":"54",
                              "AM":"374",
                              "AW":"297",
                              "AU":"61",
                              "AT":"43",
                              "AZ":"994",
                              "BS":"1",
                              "BH":"973",
                              "BD":"880",
                              "BB":"1",
                              "BY":"375",
                              "BE":"32",
                              "BZ":"501",
                              "BJ":"229",
                              "BM":"1",
                              "BT":"975",
                              "BA":"387",
                              "BW":"267",
                              "BR":"55",
                              "IO":"246",
                              "BG":"359",
                              "BF":"226",
                              "BI":"257",
                              "KH":"855",
                              "CM":"237",
                              "CA":"1",
                              "CV":"238",
                              "KY":"345",
                              "CF":"236",
                              "TD":"235",
                              "CL":"56",
                              "CN":"86",
                              "CX":"61",
                              "CO":"57",
                              "KM":"269",
                              "CG":"242",
                              "CK":"682",
                              "CR":"506",
                              "HR":"385",
                              "CU":"53",
                              "CY":"537",
                              "CZ":"420",
                              "DK":"45",
                              "DJ":"253",
                              "DM":"1",
                              "DO":"1",
                              "EC":"593",
                              "EG":"20",
                              "SV":"503",
                              "GQ":"240",
                              "ER":"291",
                              "EE":"372",
                              "ET":"251",
                              "FO":"298",
                              "FJ":"679",
                              "FI":"358",
                              "FR":"33",
                              "GF":"594",
                              "PF":"689",
                              "GA":"241",
                              "GM":"220",
                              "GE":"995",
                              "DE":"49",
                              "GH":"233",
                              "GI":"350",
                              "GR":"30",
                              "GL":"299",
                              "GD":"1",
                              "GP":"590",
                              "GU":"1",
                              "GT":"502",
                              "GN":"224",
                              "GW":"245",
                              "GY":"595",
                              "HT":"509",
                              "HN":"504",
                              "HU":"36",
                              "IS":"354",
                              "IN":"91",
                              "ID":"62",
                              "IQ":"964",
                              "IE":"353",
                              "IL":"972",
                              "IT":"39",
                              "JM":"1",
                              "JP":"81",
                              "JO":"962",
                              "KZ":"77",
                              "KE":"254",
                              "KI":"686",
                              "KW":"965",
                              "KG":"996",
                              "LV":"371",
                              "LB":"961",
                              "LS":"266",
                              "LR":"231",
                              "LI":"423",
                              "LT":"370",
                              "LU":"352",
                              "MG":"261",
                              "MW":"265",
                              "MY":"60",
                              "MV":"960",
                              "ML":"223",
                              "MT":"356",
                              "MH":"692",
                              "MQ":"596",
                              "MR":"222",
                              "MU":"230",
                              "YT":"262",
                              "MX":"52",
                              "MC":"377",
                              "MN":"976",
                              "ME":"382",
                              "MS":"1",
                              "MA":"212",
                              "MM":"95",
                              "NA":"264",
                              "NR":"674",
                              "NP":"977",
                              "NL":"31",
                              "AN":"599",
                              "NC":"687",
                              "NZ":"64",
                              "NI":"505",
                              "NE":"227",
                              "NG":"234",
                              "NU":"683",
                              "NF":"672",
                              "MP":"1",
                              "NO":"47",
                              "OM":"968",
                              "PK":"92",
                              "PW":"680",
                              "PA":"507",
                              "PG":"675",
                              "PY":"595",
                              "PE":"51",
                              "PH":"63",
                              "PL":"48",
                              "PT":"351",
                              "PR":"1",
                              "QA":"974",
                              "RO":"40",
                              "RW":"250",
                              "WS":"685",
                              "SM":"378",
                              "SA":"966",
                              "SN":"221",
                              "RS":"381",
                              "SC":"248",
                              "SL":"232",
                              "SG":"65",
                              "SK":"421",
                              "SI":"386",
                              "SB":"677",
                              "ZA":"27",
                              "GS":"500",
                              "ES":"34",
                              "LK":"94",
                              "SD":"249",
                              "SR":"597",
                              "SZ":"268",
                              "SE":"46",
                              "CH":"41",
                              "TJ":"992",
                              "TH":"66",
                              "TG":"228",
                              "TK":"690",
                              "TO":"676",
                              "TT":"1",
                              "TN":"216",
                              "TR":"90",
                              "TM":"993",
                              "TC":"1",
                              "TV":"688",
                              "UG":"256",
                              "UA":"380",
                              "AE":"971",
                              "GB":"44",
                              "US":"1",
                              "UY":"598",
                              "UZ":"998",
                              "VU":"678",
                              "WF":"681",
                              "YE":"967",
                              "ZM":"260",
                              "ZW":"263",
                              "BO":"591",
                              "BN":"673",
                              "CC":"61",
                              "CD":"243",
                              "CI":"225",
                              "FK":"500",
                              "GG":"44",
                              "VA":"379",
                              "HK":"852",
                              "IR":"98",
                              "IM":"44",
                              "JE":"44",
                              "KP":"850",
                              "KR":"82",
                              "LA":"856",
                              "LY":"218",
                              "MO":"853",
                              "MK":"389",
                              "FM":"691",
                              "MD":"373",
                              "MZ":"258",
                              "PS":"970",
                              "PN":"872",
                              "RE":"262",
                              "RU":"7",
                              "BL":"590",
                              "SH":"290",
                              "KN":"1",
                              "LC":"1",
                              "MF":"590",
                              "PM":"508",
                              "VC":"1",
                              "ST":"239",
                              "SO":"252",
                              "SJ":"47",
                              "SY":"963",
                              "TW":"886",
                              "TZ":"255",
                              "TL":"670",
                              "VE":"58",
                              "VN":"84",
                              "VG":"284",
                              "VI":"340"]
    if countryDictionary[country] != nil {
        return "\(countryDictionary[country]!)"
    } else {
        return ""
    }
    
}



extension String {
//    func removeSpecialCharsFromString(str: String) -> String {
    func removeSpecialCharsFromString() -> String {
        let chars = Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_")
        return String(self.filter { chars.contains($0) })
    }
}
